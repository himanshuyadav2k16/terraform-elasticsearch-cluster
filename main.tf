resource "aws_iam_role" "elasticsearch_iam_role" {
  name = "elasticsearch_iam_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "elasticsearch_iam_policy_document" {
  statement {
    sid = "1"

    actions = [
    "s3:PutObject",
    "s3:GetObject",
    "ec2:DescribeInstances"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "elasticsearch_iam_policy" {
  name   = "elasticsearch_iam_policy"
  policy = data.aws_iam_policy_document.elasticsearch_iam_policy_document.json
}

resource "aws_iam_role_policy_attachment" "elasticsearch_iam_role_policy" {
  role       = "${aws_iam_role.elasticsearch_iam_role.name}"
  policy_arn = "${aws_iam_policy.elasticsearch_iam_policy.arn}"
}

resource "aws_iam_instance_profile" "elasticsearch_profile" {
  name  = "elasticsearch_profile"
  role = "${aws_iam_role.elasticsearch_iam_role.name}"
}


resource "aws_s3_bucket" "mybucket" {
  bucket = "my-escluster-02"
  acl    = "private"

  tags = {
    Name        = "elasticseach"
  }
}


resource "aws_instance" "node1" {
  ami           = "ami-0a0ad6b70e61be944"
  instance_type = "t2.micro"
  private_ip = "10.0.0.10"
  iam_instance_profile = "${aws_iam_instance_profile.elasticsearch_profile.name}"
  subnet_id = aws_subnet.public_1.id
  vpc_security_group_ids = [aws_security_group.elasticsearch_cluster_sg.id]
  user_data = "${file("elasticsearch-node1-setup.sh")}"
}

resource "aws_instance" "node2" {
  ami           = "ami-0a0ad6b70e61be944"
  instance_type = "t2.micro"
  private_ip = "10.0.1.11"
  iam_instance_profile = "${aws_iam_instance_profile.elasticsearch_profile.name}"
  subnet_id = aws_subnet.public_2.id
  vpc_security_group_ids = [aws_security_group.elasticsearch_cluster_sg.id]
  user_data = "${file("elasticsearch-node2-setup.sh")}"
 
}

resource "aws_instance" "node3" {
  ami           = "ami-0a0ad6b70e61be944"
  instance_type = "t2.micro"
  private_ip = "10.0.2.12"
  iam_instance_profile = "${aws_iam_instance_profile.elasticsearch_profile.name}"
  subnet_id = aws_subnet.public_3.id
  vpc_security_group_ids = [aws_security_group.elasticsearch_cluster_sg.id]
  user_data = "${file("elasticsearch-node2-setup.sh")}"
}