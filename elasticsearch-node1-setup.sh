#!/bin/bash

# Script used to set up a new node inside an Elasticsearch cluster in AWS

# Elasticsearch Installation
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch

cat >/etc/yum.repos.d/elasticsearch.repo <<EOL
[elasticsearch]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=0
autorefresh=1
type=rpm-md
EOL

sudo yum install -y --enablerepo=elasticsearch elasticsearch 

# Discovery EC2 plugin is used for the nodes to create the cluster in AWS
echo -e "y" | /usr/share/elasticsearch/bin/elasticsearch-plugin install discovery-ec2

# Setup certificates for Transport and HTTP Layer
echo -en '\n\n' | /usr/share/elasticsearch/bin/elasticsearch-certutil ca
echo -en '\n\n\n' | /usr/share/elasticsearch/bin/elasticsearch-certutil cert --ca /usr/share/elasticsearch/elastic-stack-ca.p12


chmod 660 /usr/share/elasticsearch/elastic-certificates.p12
aws s3 cp /usr/share/elasticsearch/elastic-certificates.p12 s3://my-escluster-02/elastic-certificates.p12

cp  /usr/share/elasticsearch/elastic-certificates.p12 /etc/elasticsearch/elastic-certificates.p12

# Setup Xms & Xmx according to your VM config.
echo ES_JAVA_OPTS="\"-Xms512m -Xmx512m\"" >> /etc/sysconfig/elasticsearch
echo MAX_LOCKED_MEMORY=unlimited >> /etc/sysconfig/elasticsearch


# configuration for Elasticsearch nodes to find each other
echo "cluster.name: my-application" >> /etc/elasticsearch/elasticsearch.yml
echo "network.host: [_local_,_site_]" >> /etc/elasticsearch/elasticsearch.yml
echo "discovery.seed_providers: ec2" >> /etc/elasticsearch/elasticsearch.yml
echo "discovery.ec2.endpoint: ec2.us-east-2.amazonaws.com" >> /etc/elasticsearch/elasticsearch.yml
echo "cluster.initial_master_nodes: [10.0.0.10,10.0.1.11,10.0.2.12]" >> /etc/elasticsearch/elasticsearch.yml
echo "cloud.node.auto_attributes: true" >> /etc/elasticsearch/elasticsearch.yml
echo "cluster.routing.allocation.awareness.attributes: aws_availability_zone" >> /etc/elasticsearch/elasticsearch.yml

# Enable Transport TLS/SSL encryption
echo "xpack.security.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
echo "xpack.security.transport.ssl.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
echo "xpack.security.transport.ssl.verification_mode: certificate" >> /etc/elasticsearch/elasticsearch.yml
echo "xpack.security.transport.ssl.keystore.path: elastic-certificates.p12" >> /etc/elasticsearch/elasticsearch.yml
echo "xpack.security.transport.ssl.truststore.path: elastic-certificates.p12" >> /etc/elasticsearch/elasticsearch.yml
# Enable Http TLS/SSL encryption
echo "xpack.security.http.ssl.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
echo "xpack.security.http.ssl.keystore.path: elastic-certificates.p12" >> /etc/elasticsearch/elasticsearch.yml
echo "xpack.security.http.ssl.truststore.path: elastic-certificates.p12" >> /etc/elasticsearch/elasticsearch.yml
echo "xpack.security.http.ssl.client_authentication: optional" >> /etc/elasticsearch/elasticsearch.yml


systemctl enable elasticsearch
systemctl start elasticsearch

echo -e "y" | /usr/share/elasticsearch/bin/elasticsearch-setup-passwords auto >> ~/es-pass.txt
