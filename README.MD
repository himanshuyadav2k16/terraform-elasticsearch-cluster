# Elasticsearch cluster with Terraform

This document describes how to set up an elasticsearch cluster of EC2 instances using Terraform with secuirty enabled(encrypted communication and authentication).

### Resources Required
- Terraform
- AWS account (only free tier resources are used for this)

### Encrypted communication and authentication
The Elasticsearch security features enable you to secure a cluster. You can password-protect your data as well as implement more advanced security measures such as encrypting communications.

1. xpack.security.enabled to be set true.
2. Encrypting communications between nodes in a cluster(Required)
    - Generate node certificates.
    - Enable TLS and specify the information required to access the node’s certificate.
        ```
        xpack.security.transport.ssl.enabled: true
        xpack.security.transport.ssl.verification_mode: certificate 
        xpack.security.transport.ssl.keystore.path: elastic-certificates.p12 
        xpack.security.transport.ssl.truststore.path: elastic-certificates.p12 
        ```

3. Encrypting HTTP client communications(optional)
    -  Generate node certificates or use same certificates used in TLS.
        ```
        xpack.security.http.ssl.enabled: true
        xpack.security.http.ssl.keystore.path: elastic-certificates.p12
        xpack.security.http.ssl.truststore.path: elastic-certificates.p12
        xpack.security.http.ssl.client_authentication: optional
        ```
4. Set the passwords for all built-in users
    -  elasticsearch-setup-passwords auto 

### EC2 Discovery Plugin

The [EC2 Discovery Pluging] is used by Elasticsearch to find nodes in AWS. The EC2 discovery plugin uses the AWS API for discovery.
 So, an IAM Role is necessary for this plugin to find other nodes
in the cluster.

Since we are using Terraform, we will need to create a few things for our cluster to work properly. First of all, our cluster will have 3 instances running Elasticsearch 7.10 + EC2 Discovery Plugin. Also, this instance needs to be able to call the AWS API to find other nodes, for that we will use an IAM Role. Those instances will stay under a security group
named **elasticsearch_cluster_sg** that has exposed port 22, 9200, and 9300 (for SSH, Elasticsearch API endpoint and Elasticsearch 
internal communication, respectively).

### Attaching roles to instances with Terraform
we create an `aws_iam_instance_profile` that will reference our IAM Role `elasticsearch_iam_role`. Our instances can reference `iam_instance_profile` and will be able to reach out to the AWS API.

##VPC setup
we have configured our own VPC for this cluster that includes 3 public subnets, internet gateway, route table, and oute_table_association.

##S3 Bucket
for certificates to be shared across different nodes we have saved the certs on the S3 bucket.

## Setup Script
The `elasticsearch-node1-setup.sh` scripts installs Elasticsearch 7.10  on Amazon linux 2 VM.
Also, it configures Transport TLS/SSL encryption, HTTP TLS/SSL encryption, user auth, and some other flags in order for the node to be properly setup.

All those flags are set in `/etc/elasticsearch/elasticsearch.yml`.

node1 uses `elasticsearch-node1-setup.sh` and node2 & node3 uses `elasticsearch-node2-setup.sh` as both are different as we are generating certificates on node1 only and using them across node2 & node3.

### Monitoring 
curl -u 'elastic:password' https://127.0.0.1:9200/_cluster/health?pretty
elastic user password can be found inside node1 VM ~/es-pass.txt

## Terraform 
Configure AWS access_key and secret_key before running terraform cmd
`terraform int`

`terraform plan`

`terraform apply`

