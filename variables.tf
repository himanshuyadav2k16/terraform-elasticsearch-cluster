variable "region" {
  type = string
  description = "AWS Region, where to deploy ELK cluster"
  default = "us-east-2"
}

variable "access_key" {
  type = string
  description = "Access key of your aws account"
}
variable "secret_key" {
  type = string
  description = "Secret key of your aws account"
}

data "aws_availability_zones" "available" {
  state = "available"
}
